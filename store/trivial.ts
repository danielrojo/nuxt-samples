import { Module, VuexModule, Mutation } from 'vuex-module-decorators'
import { Card } from '~/model/Card'

@Module({
    name: 'trivial',
    stateFactory: true,
    namespaced: true
})
export default class User extends VuexModule {
    public storedCards: Card[] = []

    @Mutation
    public updateCards(cards: Card[]) {
        this.storedCards = cards
    }

    @Mutation
    public updateCard(card: Card) {
        this.storedCards.splice(card.index, 1, card)
    }
}
