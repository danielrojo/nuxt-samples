import { Module, VuexModule, Mutation } from 'vuex-module-decorators'

@Module({
    name: 'range',
    stateFactory: true,
    namespaced: true
})
export default class User extends VuexModule {
    public abvRange: number[] = [0,5]

    get currentRange(): number[] {
        return this.abvRange
    }
    @Mutation
    public updateRange(data: number[]) {
        this.abvRange = data
    }
}
