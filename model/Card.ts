export class Card {
    question: string;
    answers: string[] = [];
    correctAnswer: string;
    answered = false;
    answer = '';
    index = -1;

    constructor(json?: any, index = -1) {
        if (json !== undefined) {
            this.index = index;
            this.question = json.question;
            this.answers = json.incorrect_answers;
            this.answers = [...this.answers, json.correct_answer];
            this.correctAnswer = json.correct_answer;
        } else {
            this.question = '';
            this.answers = [];
            this.correctAnswer = '';
        }
    }
}

export default Card;