import { mount } from '@vue/test-utils'
import Trivial from '@/components/Trivial'

describe('Trivial', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(Trivial)
    expect(wrapper.vm).toBeTruthy()
  })
})
